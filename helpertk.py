#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, string

import tkinter as tk
from tkinter import ttk

COL_BUTTONFACE	= "#bbc1ae"
COL_FG			= "#000000"
COL_WINDOWBG	= "#deebc5"

class SearchableTreeview(ttk.Treeview):
	def __init__(self, *args, **kwargs):
		ttk.Treeview.__init__(self, *args, **kwargs)
		#create the entry on init but does no show it
		self._toSearch = tk.StringVar()
		self.entry = tk.Entry(self, textvariable=self._toSearch)

		self.bind("<KeyPress>", self._keyOnTree)
		self._toSearch.trace_variable("w", self._search)
		self.entry.bind("<Return>", self._hideEntry)
		self.entry.bind("<Escape>", self._hideEntry)

	def _keyOnTree(self, event):
		self.entry.place(relx=1, anchor=tk.NE)
		if event.char.isalpha():
			self.entry.insert(tk.END, event.char)
		self.entry.focus_set()

	def _hideEntry(self, event):
		self.entry.delete(0, tk.END)
		self.entry.place_forget()
		self.focus_set()

	def _search(self, *args):
		pattern = self._toSearch.get()
		#avoid search on empty string
		if len(pattern) > 0:
			self.search(pattern)

	def search(self, pattern, item=''):
		children = self.get_children(item)
		for child in children:
			text = self.item(child, 'text')
			if text.lower().startswith(pattern.lower()):
				self.selection_set(child)
				self.see(child)
				return True
			else:
				res = self.search(pattern, child)
				if res:
					return True

def center(win, dual_monitor_system=False):
	"""
	centers a tkinter window
	:param win: the root or Toplevel window to center
	"""
	win.update_idletasks()
	width = win.winfo_width()
	frm_width = win.winfo_rootx() - win.winfo_x()
	win_width = width + 2 * frm_width
	height = win.winfo_height()
	titlebar_height = win.winfo_rooty() - win.winfo_y()
	win_height = height + titlebar_height + frm_width
	screenwidth = win.winfo_screenwidth()
	pointerx = win.winfo_pointerx()
	if dual_monitor_system:
		if pointerx<(screenwidth//2):
			x = (win.winfo_screenwidth()//2)//2 - win_width//2	# left monitor
		else:
			x = (screenwidth + \
				screenwidth//2) // 2 - win_width // 2		# right monitor
	else:
		x = win.winfo_screenwidth() // 2 - win_width // 2	# left monitor
	y = win.winfo_screenheight() // 2 - win_height // 2
	win.geometry('%sx%s+%s+%s'%(width, height, x, y))
	win.deiconify()

def apply_grid_options(obj, args):
	# 0 column
	# 1 row
	# 2 columnspan
	# 3 rowspan
	# 4 sticky
	if len(args)==3:
		obj.grid(column=args[0], row=args[1], columnspan=args[2])
	elif len(args)==4:
		obj.grid(column=args[0], row=args[1], columnspan=args[2], \
			rowspan=args[3])
	elif len(args)==5:
		obj.grid(column=args[0], row=args[1], columnspan=args[2], \
			rowspan=args[3], sticky=args[4])
	else:
		obj.grid(column=args[0], row=args[1])

def make_button(master, *args, **keywords):
	res = tk.Button(master, fg=COL_FG, activebackground=COL_BUTTONFACE, bd=1)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	#~ res.grid(padx=2, pady=2)
	return res

def make_label(master, *args, **keywords):
	res = tk.Label(master, fg=COL_FG, activebackground=COL_BUTTONFACE, bd=1)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def make_checkbutton(master, *args, **keywords):
	res = tk.Checkbutton(master, fg=COL_FG, activebackground=COL_BUTTONFACE, \
		bd=1, highlightbackground=COL_BUTTONFACE
	)
	#~ print(dump_colors(res))
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def make_spinbox(master, *args, **keywords):
	res = tk.Spinbox(master, fg=COL_FG, bg=COL_WINDOWBG, bd=1, \
		highlightcolor=COL_BUTTONFACE, highlightthickness=0, \
		repeatdelay=500
	)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def make_entry(master, *args, **keywords):
	res = tk.Entry(master, fg=COL_FG, bg=COL_WINDOWBG, bd=1, \
		highlightcolor=COL_BUTTONFACE)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def make_text(master, *args, **keywords):
	res = tk.Text(master, fg=COL_FG, bg=COL_WINDOWBG, bd=1, \
		highlightcolor=COL_BUTTONFACE, highlightthickness=0, wrap=tk.WORD
	)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	#~ res.grid(padx=4, pady=4)
	return res

def make_labelframe(master, *args, **keywords):
	res = tk.LabelFrame(master, fg=COL_FG)
	#~ res.grid(column=args[0], row=args[1])
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	#~ res.grid(padx=4, pady=4)
	return res

def make_frame(master, *args, **keywords):
	res = tk.Frame(master)
	#~ res.grid(column=args[0], row=args[1])
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	#~ res.grid(padx=4, pady=4)
	return res

def make_ttk_treeview(master, *args, **keywords):
	res = ttk.Treeview(master)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def make_ttk_searchable_treeview(master, *args, **keywords):
	res = SearchableTreeview(master)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def make_ttk_scrollbar(master, *args, **keywords):
	res = ttk.Scrollbar(master)
	apply_grid_options(res, args)
	for k,v in keywords.items():
		res[k] = v
	return res

def anim_b_press(obj):
	obj.config(relief=tk.SUNKEN)
	obj.after(200,
		lambda: obj.config(relief=tk.RAISED)
	)
	return obj

def helpertk_main():
	print(len(sys.argv))
	if len(sys.argv)>1:
		a = sys.argv[1]

if __name__=='__main__':
	helpertk_main()
